package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class downloadAssignment extends AppCompatActivity {

    String groupId,assignmentId;
    RecyclerView recyclerView;
    DatabaseReference reference;
    ArrayList<uploadedFile> mList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_assignment);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Download Assignment");
        recyclerView=findViewById(R.id.downloadRecyclerView);
        mList=new ArrayList<uploadedFile>();
        Bundle b=getIntent().getExtras();
        groupId=b.getString("groupId");
        assignmentId=b.getString("assignmentId");
        reference= FirebaseDatabase.getInstance().getReference("uploadedFile").child(groupId).child(assignmentId);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        downloadAdapter AssignmentAdapter=new downloadAdapter(downloadAssignment.this,mList,groupId);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(AssignmentAdapter);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    uploadedFile message = snapshot.getValue(uploadedFile.class);
                    mList.add(message);
                }
                LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
                downloadAdapter AssignmentAdapter=new downloadAdapter(downloadAssignment.this,mList,groupId);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(AssignmentAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}