package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private Button add,vGroup;
    private TextView war,load;
    private ArrayList<String> groupNameList,groupIdList;
    private RecyclerView recycle;
    private DatabaseReference reference2;
    private ArrayList<Group> gList;
    GroupAdapter groupAdapter;
    String teacherId;
    String userName;
    String gp;
    String type="";
    Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Home Screen");
        Bundle b=getIntent().getExtras();
        teacherId=b.getString("teacherId");
        userName=b.getString("userName");
        type=b.getString("type");
        reference2 = FirebaseDatabase.getInstance().getReference("groups").child(teacherId);
        add = findViewById(R.id.addGroup);
        recycle = findViewById(R.id.recycler_view_home);
        groupNameList = new ArrayList<>();
        groupIdList = new ArrayList<>();
        gList= new ArrayList<>();
        groupAdapter = new GroupAdapter(HomeActivity.this, groupNameList, groupIdList,userName,type);
        recycle.setAdapter(groupAdapter);
        recycle.setLayoutManager(new LinearLayoutManager(HomeActivity.this));

        reference2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    gList.add(postSnapshot.getValue(Group.class));

                }
                groupNameList.clear();
                groupIdList.clear();

                for (int i=0;i< gList.size();i++){
                    groupIdList.add(gList.get(i).getId());
                    groupNameList.add(gList.get(i).getName());
                }
                groupAdapter = new GroupAdapter(HomeActivity.this, groupNameList, groupIdList,userName,type);
                recycle.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
                recycle.setAdapter(groupAdapter);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(HomeActivity.this, databaseError.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void addGroups(View view) {

        Intent i=new Intent(HomeActivity.this, AddGroupActivity.class);
        i.putExtra("teacherId",teacherId);
        startActivity(i);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logoutBtn:
                // User chose the "Settings" item, show the app settings UI...

                new AlertDialog.Builder(HomeActivity.this)
                        .setTitle("LogOut")
                        .setMessage("Do you really want to logOut")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Intent i=new Intent(HomeActivity.this,MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                        })
                        // A null listener allows the button to dismiss the dialog and take no further action.

                        .show();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
