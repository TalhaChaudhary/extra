package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AnnouncementActivity extends AppCompatActivity {

    Button post;
    RecyclerView recyclerView;
    ArrayList<Announcement> mList;
    DatabaseReference reference;
    String type="";
    String groupId="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Announcement Activity");
        post=findViewById(R.id.btn_post);
        recyclerView=findViewById(R.id.recyclerview);
        mList=new ArrayList<Announcement>();
        Bundle b=getIntent().getExtras();
        type=b.getString("type");
        if(type.equals("student"))
        {
            post.setVisibility(View.GONE);
        }
        groupId=b.getString("groupId");
        reference= FirebaseDatabase.getInstance().getReference("Announcement").child(groupId);
        setList();
        LinearLayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        AnnouncementAdapter announcementAdapter=new AnnouncementAdapter(mList,getApplicationContext(),type,groupId);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(announcementAdapter);
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(AnnouncementActivity.this,PostAnnouncementActivity.class);
                i.putExtra("groupId",groupId);
                startActivity(i);
                finish();
                //Toast.makeText(getApplicationContext(),mList.size()+"",Toast.LENGTH_LONG).show();
                //System.out.print(mList.size());
            }
        });
    }
    private void setList() {
        //here we get the group Id from Intents

        reference.addListenerForSingleValueEvent(listener);
    }
    ValueEventListener listener=new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            ArrayList<Announcement> ab=new ArrayList<Announcement>();
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                Announcement a = snapshot.getValue(Announcement.class);
                mList.add(a);
            }
           // mList=ab;
            AnnouncementAdapter adptr=new AnnouncementAdapter(mList,getApplicationContext(),type,groupId);
            LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
            recyclerView.setAdapter(adptr);
            recyclerView.setLayoutManager(linearLayoutManager);
        }
        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            //list.add(new Message("haseeb1","cancel ho rha hee"));
        }
    };
}