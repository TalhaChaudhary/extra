package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText email, password, idStud, passwordStd;
    private String emailStr, passwordStr, idStrStud, passwordStrStd, name = null;
    private Button loginTec, loginStd, stdBtn, techBtn, createAccBtn;
    private DatabaseReference reference, refrenceStd;
    private FirebaseAuth auth;
    private ArrayList<teacher> listTecher;
    private RelativeLayout r;
    private boolean flag=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Login");

        reference = FirebaseDatabase.getInstance().getReference("teachers");
        refrenceStd = FirebaseDatabase.getInstance().getReference("students");

        listTecher = new ArrayList<>();

        auth = FirebaseAuth.getInstance();
        email = findViewById(R.id.EmailFielsLogin);
        password = findViewById(R.id.passwordFeildLogin);
        idStud = findViewById(R.id.IdFielsLoginStd);
        passwordStd = findViewById(R.id.passwordFeildLoginStd);
        loginStd = findViewById(R.id.loginStd);
        loginTec = findViewById(R.id.login);
        createAccBtn = findViewById(R.id.createAccountTech);
        stdBtn = findViewById(R.id.stdBtn);
        techBtn = findViewById(R.id.teacherBtn);
        r= findViewById(R.id.reletiveMain);


    }


    public void loginFun(View view) {


        emailStr = email.getText().toString();
        if (emailStr.isEmpty()) {
            email.setHint("Please enter Email");
            email.setHintTextColor(getResources().getColor(R.color.warningClr));
        }
        else {
            email.setHint("");

            passwordStr = password.getText().toString();
            reference.addListenerForSingleValueEvent(listener);
        }

    }

    ValueEventListener listener = new ValueEventListener() {


        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                listTecher.add(postSnapshot.getValue(teacher.class));

            }
            listTecher.get(0);

            for (int i = 0; i < listTecher.size(); i++) {
                if (listTecher.get(i).getEmail().equals(emailStr)) {
                    name = listTecher.get(i).getName();
                    flag=true;
                }
            }
            if (flag==true) {
                String pass = dataSnapshot.child(name).child("password").getValue(String.class);
                String emaill = dataSnapshot.child(name).child("name").getValue(String.class);
                String uid=dataSnapshot.child(name).child("uid").getValue(String.class);
                flag=false;
                if (pass.equals(passwordStr)) {

                    Intent i = new Intent(MainActivity.this, HomeActivity.class);
                    i.putExtra("teacherId",uid);
                    i.putExtra("userName",emaill);
                    i.putExtra("type","teacher");
                    startActivity(i);
                    email.setText("");
                    password.setText("");
                    password.setHint("Enter Password");
                    password.setHintTextColor(getResources().getColor(R.color.greyClr));
                    email.setHint("Enter Email");
                    email.setHintTextColor(getResources().getColor(R.color.greyClr));
                    finish();

                } else {
                    password.setText("");
                    password.setHint("Incorrect Password");
                    password.setHintTextColor(getResources().getColor(R.color.warningClr));

                }
            } else {
                email.setText("");
                email.setHint("Invalid Email");
                email.setHintTextColor(getResources().getColor(R.color.warningClr));

            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Toast.makeText(MainActivity.this, databaseError.toString(), Toast.LENGTH_SHORT).show();
        }
    };
    public void createAcc (View view){
        startActivity(new Intent(MainActivity.this,signupActivity.class));
    }
    public void TechBtun (View view){
        r.setBackground(getDrawable(R.drawable.teacherimge));
        stdBtn.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.unselected));
        techBtn.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.selected));
        password.setVisibility(View.VISIBLE);
        email.setVisibility(View.VISIBLE);
        createAccBtn.setVisibility(View.VISIBLE);
        idStud.setVisibility(View.INVISIBLE);
        passwordStd.setVisibility(View.INVISIBLE);
        loginTec.setVisibility(View.VISIBLE);
        loginStd.setVisibility(View.INVISIBLE);
        //r.setBackground(getResources().getDrawable(R.drawable.imageteacherlogin));
        idStud.setText("");
        passwordStd.setText("");

    }
    public void StdButn (View view){
        r.setBackground(getDrawable(R.drawable.studentimage));
        stdBtn.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.selected));
        techBtn.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.unselected));

        password.setVisibility(View.INVISIBLE);
        email.setVisibility(View.INVISIBLE);
        createAccBtn.setVisibility(View.INVISIBLE);
        idStud.setVisibility(View.VISIBLE);
        passwordStd.setVisibility(View.VISIBLE);
        loginTec.setVisibility(View.INVISIBLE);
        loginStd.setVisibility(View.VISIBLE);
        //r.setBackground(getResources().getDrawable(R.drawable.imagestudentlogin));
        email.setText("");
        password.setText("");

    }

    public void loginFunStd(View view) {
        passwordStd.setBackgroundColor(getResources().getColor(R.color.original));

        idStrStud = idStud.getText().toString();
        if(idStrStud.isEmpty()){
            idStud.setHint("Please Enter Id");
            idStud.setHintTextColor(getResources().getColor(R.color.warningClr));
        }
        else{
            idStud.setHint("Enter Id");

            passwordStrStd = passwordStd.getText().toString();
            refrenceStd.child(idStrStud)
                    .addListenerForSingleValueEvent(listener2);
        }
    }
    ValueEventListener listener2 = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if(dataSnapshot.exists()){
                String sid=dataSnapshot.child("stdId").getValue(String.class);
                String passS=dataSnapshot.child("password").getValue(String.class);
                String name=dataSnapshot.child("name").getValue(String.class);
                String groupId=dataSnapshot.child("grpId").getValue(String.class);
                if(passS.equals(passwordStrStd)){

                    Intent i=new Intent(MainActivity.this,GroupActivity.class);
                    i.putExtra("suid",sid);
                    i.putExtra("type","student");
                    i.putExtra("userName",name);
                    i.putExtra("groupId",groupId);
                    startActivity(i);
                    idStud.setText("");
                    passwordStd.setText("");
                    passwordStd.setHint("Enter Password");
                    finish();
                    //passwordStd.setHintTextColor(getResources().getColor(R.color.greyClr));
                }
                else{
                    passwordStd.setText("");
                    passwordStd.setHint("Incorrect Password");
                    passwordStd.setHintTextColor(getResources().getColor(R.color.warningClr));

                    Toast.makeText(MainActivity.this, "Wrong Password", Toast.LENGTH_SHORT).show();
                }
            }
            else{
                idStud.setText("");
                idStud.setHint("Incorrect Enter Id");
                idStud.setHintTextColor(getResources().getColor(R.color.warningClr));
                Toast.makeText(MainActivity.this, "Record Not Found", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Toast.makeText(MainActivity.this, databaseError.toString(), Toast.LENGTH_SHORT).show();
        }
    };
}


