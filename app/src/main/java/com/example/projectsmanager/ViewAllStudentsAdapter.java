package com.example.projectsmanager;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class ViewAllStudentsAdapter extends RecyclerView.Adapter<ViewAllStudentsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList stdnameList,stdPasswordList,stdIdList;
    String groupId="",groupName="";

    public ViewAllStudentsAdapter(Context context, ArrayList stdnameList, ArrayList stdPasswordList, ArrayList stdIdList, String groupId, String groupName) {
        this.context = context;
        this.stdnameList = stdnameList;
        this.stdPasswordList = stdPasswordList;
        this.stdIdList = stdIdList;
        this.groupId = groupId;
        this.groupName = groupName;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(context);
        View view= inflater.inflate(R.layout.my_students_row,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.stdName.setText(String.valueOf(stdnameList.get(position)));
        holder.stdPassword.setText(String.valueOf(stdPasswordList.get(position)));
        holder.stdId.setText(String.valueOf(stdIdList.get(position)));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence options[] = new CharSequence[]
                        {
                                "Remove"
                        };
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Remove Student");
                builder.setItems(options, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        if (i == 0)
                        {
                            DatabaseReference reference= FirebaseDatabase.getInstance().getReference("students");
                            reference.child(String.valueOf(stdIdList.get(position)))
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>()
                                    {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if (task.isSuccessful())
                                            {
                                                Intent i=new Intent(context,ViewStudentActivity.class);
                                                i.putExtra("grpId",groupId);
                                                i.putExtra("grpName",groupName);
                                                context.startActivity(i);
                                                ((Activity)context).finish();
                                                //stdIdList.remove(position);
                                                //Toast.makeText(context,"Student Remove SucessFully...!",Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                        }
                    }
                });
                builder.show();

            }
        });


    }

    @Override
    public int getItemCount() {
        return stdIdList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView stdName,stdPassword,stdId;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            stdName=itemView.findViewById(R.id.allStdNameRow);
            stdPassword=itemView.findViewById(R.id.allStdPasswordRow);
            stdId=itemView.findViewById(R.id.allStdIdRow);

        }
    }
}
