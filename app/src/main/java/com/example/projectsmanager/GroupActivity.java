package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class GroupActivity extends AppCompatActivity {

    DatabaseReference reference,refernceForNotification;
    RecyclerView recyclerView;
    Button send,viewAnnouncement,viewAssignment,viewStudent,logoutStudent;
    RelativeLayout l1;
    ArrayList<Message> list;
    EditText textSend;
    String ifStudent="";
    String suid="";
    LinearLayout linearLayout;
    int assignMentCount=0;
    private String groupIdvalye,groupNameValue,userName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        Bundle b=getIntent().getExtras();
        groupIdvalye=b.getString("groupId");
        groupNameValue=b.getString("groupName");
        userName=b.getString("userName");
        ifStudent=b.getString("type");
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Group Chat");
        viewStudent=findViewById(R.id.viewStudents);
        recyclerView=findViewById(R.id.recycler_view);
        send=findViewById(R.id.btn_send);
        textSend=findViewById(R.id.text_send);
        viewAnnouncement=findViewById(R.id.viewAnnouncement);
        viewAssignment=findViewById(R.id.viewAssignment);

        if(ifStudent.equals("student"))
        {
            suid=b.getString("suid");
            viewStudent.setVisibility(View.GONE);
            //logoutStudent.setVisibility(View.VISIBLE);
            //l1.setVisibility(View.VISIBLE);
        }

        final String groupId=groupIdvalye;
        list=new ArrayList<>();
        reference= FirebaseDatabase.getInstance().getReference("Chats").child(groupId);
        refernceForNotification= FirebaseDatabase.getInstance().getReference("Assignments").child(groupId);
        setList();
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               final Message m=new Message(textSend.getText().toString(),userName,new Date());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
                String format = simpleDateFormat.format(new Date());
                reference.child("message"+format).setValue(m).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(),"Text is not added to firebase",Toast.LENGTH_LONG).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        list.add(m);
                        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
                        linearLayoutManager.setStackFromEnd(true);
                        MessageAdapter messageAdapter=new MessageAdapter(GroupActivity.this,list,userName);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setAdapter(messageAdapter);
                    }
                });
                textSend.setText("");
            }
        });
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        MessageAdapter messageAdapter=new MessageAdapter(GroupActivity.this,list,userName);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(messageAdapter);
        viewAnnouncement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(GroupActivity.this,AnnouncementActivity.class);
                i.putExtra("groupId",groupId);
                i.putExtra("type",ifStudent);
                startActivity(i);
            }
        });
        viewAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(GroupActivity.this,AssignmentActivity.class);
                i.putExtra("groupId",groupId);
                i.putExtra("type",ifStudent);
                i.putExtra("suid",suid);
                startActivity(i);
            }
        });
        refernceForNotification.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int count= (int) dataSnapshot.getChildrenCount();

                if(assignMentCount !=0 && count>assignMentCount )
                {
                    if(ifStudent.equals("student"))
                    {
                        new AlertDialog.Builder(GroupActivity.this)
                                .setTitle("New Assignment")
                                .setMessage("You have a new assignment please check your view assignment for further detail...")

                                // Specifying a listener allows you to take an action before dismissing the dialog.
                                // The dialog is automatically dismissed when a dialog button is clicked.
                                .setPositiveButton("okay",null)

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                
                                .show();
                    }
                }
                else{
                    assignMentCount=count;
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setList() {
        //here we get the group Id from Intents

        reference.addListenerForSingleValueEvent(listener);
    }
    ValueEventListener listener=new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            list.clear();
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                Message message = snapshot.getValue(Message.class);
                list.add(message);
            }
            Collections.sort(list, new Comparator<Message>() {
                @Override
                public int compare(Message o1, Message o2) {
                    if(o1.getDateTime()==null || o2.dateTime==null)
                        return 0;
                   return o1.getDateTime().compareTo(o2.getDateTime());
                }
            });
            LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
            linearLayoutManager.setStackFromEnd(true);
            MessageAdapter messageAdapter=new MessageAdapter(GroupActivity.this,list,userName);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(messageAdapter);

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            //list.add(new Message("haseeb1","cancel ho rha hee"));
        }
    };

    public void ViewStdFunc(View view) {
        Intent i= new Intent(GroupActivity.this,ViewStudentActivity.class);
        i.putExtra("grpId",groupIdvalye);
        i.putExtra("grpName",groupNameValue);
        startActivity(i);
    }

    /*.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                /*for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                     Message message=postSnapshot.getValue(Message.class);
                     list.add(message);
                }
    String sender=dataSnapshot.child("sender").getValue(String.class);
    String message=dataSnapshot.child("message").getValue(String.class);
                list.add(new Message(sender,message));
}

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
});*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logoutBtn:
                // User chose the "Settings" item, show the app settings UI...
                new AlertDialog.Builder(GroupActivity.this)
                        .setTitle("LogOut")
                        .setMessage("Do you really want to logOut")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Intent i=new Intent(GroupActivity.this,MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                        })
                        // A null listener allows the button to dismiss the dialog and take no further action.

                        .show();

                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if(ifStudent.equals("student"))
        {
            getMenuInflater().inflate(R.menu.logout,menu);
                        //logoutStudent.setVisibility(View.VISIBLE);
            //l1.setVisibility(View.VISIBLE);
        }

        return super.onCreateOptionsMenu(menu);
    }
}