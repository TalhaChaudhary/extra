package com.example.projectsmanager;

public class Student {
    public String name,password,stdId,grpId;

    public Student(){}
    public Student(String name, String password,String grpId,String stdId) {
        this.name = name;
        this.password = password;
        this.grpId = grpId;
        this.stdId =stdId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStdId() {
        return stdId;
    }

    public void setStdId(String stdId) {
        this.stdId = stdId;
    }

    public String getGrpId() {
        return grpId;
    }

    public void setGrpId(String grpId) {
        this.grpId = grpId;
    }
}
