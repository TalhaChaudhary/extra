package com.example.projectsmanager;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.MyViewHolder> {

    private Context context;
    private ArrayList groupNameList,groupIdList;
    String teacherName,type;

    public GroupAdapter(Context context, ArrayList groupNameList, ArrayList groupIdList, String teacherName, String type) {
        this.context = context;
        this.groupNameList = groupNameList;
        this.groupIdList = groupIdList;
        this.teacherName = teacherName;
        this.type = type;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(context);
        View view= inflater.inflate(R.layout.my_group_row,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.groupName.setText(String.valueOf(groupNameList.get(position)));
        holder.groupId.setText(String.valueOf(groupIdList.get(position)));
        final String groupIdTemp= groupIdList.get(position).toString();
        final String groupNameTemp= groupNameList.get(position).toString();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(context,GroupActivity.class);
                i.putExtra("groupId",groupIdTemp);
                i.putExtra("groupName",groupNameTemp);
                i.putExtra("userName",teacherName);
                i.putExtra("type",type);
                v.getContext().startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return groupIdList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView groupName,groupId;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            groupName=itemView.findViewById(R.id.groupNameRow);
            groupId=itemView.findViewById(R.id.groupIdRow);

        }


    }
}
