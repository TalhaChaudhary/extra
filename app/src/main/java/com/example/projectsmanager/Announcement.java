package com.example.projectsmanager;

import java.util.Date;

public class Announcement {
    String Id,description,Title;
    Date date;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Announcement() {
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Announcement(String id, String description, String title, Date date) {
        Id = id;
        this.description = description;
        Title = title;
        this.date = date;
    }
}
