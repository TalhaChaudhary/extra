package com.example.projectsmanager;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.ViewHolder> {
    ArrayList<Announcement> mList;
    Context mContext;
    String type,groupId;

    public AnnouncementAdapter(ArrayList<Announcement> mList, Context mContext, String type, String groupId) {
        this.mList = mList;
        this.mContext = mContext;
        this.type = type;
        this.groupId = groupId;
    }

    @NonNull
    @Override
    public AnnouncementAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.announcement_view, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull AnnouncementAdapter.ViewHolder holder, int position) {
               final Announcement a=mList.get(position);
                //holder.announcement.setText(a.getDescription());
                holder.title.setText(a.getTitle());
                final String dateTime=a.getDate().toString().substring(0,10);
                holder.date.setText(dateTime);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i=new Intent(mContext,AnnouncementDetailActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("description",a.getDescription());
                        i.putExtra("title",a.getTitle());
                        i.putExtra("date",dateTime);
                        mContext.startActivity(i);
                    }
                });
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView announcement,title,date;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //announcement=itemView.findViewById(R.id.announce);
            date=itemView.findViewById(R.id.date);
            title=itemView.findViewById(R.id.AnnouncementTitle);
        }
    }
}
