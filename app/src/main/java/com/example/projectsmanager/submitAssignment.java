package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

public class submitAssignment extends AppCompatActivity {


    TextView uploaded;
    Button upload,submit;
    DatabaseReference databaseReference;
    StorageReference storageReference;
    DatabaseReference referenceForSubmissionChecking;
    String groupId,suid,assignmentId;
    Uri file;
    int status=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ProgressDialog checkingDialoug=new ProgressDialog(this);
        checkingDialoug.setTitle("Loading...!");
        checkingDialoug.setMessage("Loading");
        checkingDialoug.show();
        setContentView(R.layout.activity_submit_assignment);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Submit Assignment");
        upload=findViewById(R.id.upload);
        uploaded=findViewById(R.id.uploaded);
        submit=findViewById(R.id.submit);
        Bundle b=getIntent().getExtras();
        groupId=b.getString("groupId");
        suid=b.getString("suid");
        assignmentId=b.getString("assignmentId");
        referenceForSubmissionChecking=FirebaseDatabase.getInstance().getReference("uploadedFile").child(groupId).child(assignmentId).child(suid);
        referenceForSubmissionChecking.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    upload.setVisibility(View.GONE);
                    upload.setVisibility(View.GONE);
                    submit.setVisibility(View.GONE);
                    uploaded.setText("You have already Submit This Assignment");
                }
                checkingDialoug.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        if(ContextCompat.checkSelfPermission(submitAssignment.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat
                    .requestPermissions(
                            submitAssignment.this,
                            new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                            10);
            // Permission is not granted
        }
        storageReference= FirebaseStorage.getInstance().getReference();
        databaseReference= FirebaseDatabase.getInstance().getReference("uploadedFile").child(groupId);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPDFFile();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
           // @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                if (status == 1) {
                    String url = file.toString();
                    uploadedFile f = new uploadedFile(suid, url);
                    databaseReference.child(assignmentId).child(suid).setValue(f)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    new AlertDialog.Builder(submitAssignment.this)
                                            .setTitle("Assignment")
                                            .setMessage("Your Assignment Has been uploaded...!")

                                            // Specifying a listener allows you to take an action before dismissing the dialog.
                                            // The dialog is automatically dismissed when a dialog button is clicked.
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                    Intent i = new Intent(submitAssignment.this, AssignmentActivity.class);
                                                    i.putExtra("groupId", groupId);
                                                    i.putExtra("type", "student");
                                                    i.putExtra("suid", suid);
                                                    startActivity(i);
                                                    finish();
                                                }
                                            })

                                            // A null listener allows the button to dismiss the dialog and take no further action.

                                            .show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                }
                            });
                }
                else
                {
                    uploaded.setVisibility(View.VISIBLE);
                    uploaded.setText("Please Upload File First..!!");
                    //upload.setTextColor(Integer.parseInt("#FF0606"));
                }
            }
        });
    }

    private void selectPDFFile() {

        Intent i=new Intent();
        i.setType("application/pdf");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i,"Select pdf File...."),1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode==RESULT_OK && data!=null && data.getData()!=null)
        {
            uploadPDFFILE(data.getData());
        }
    }

    private void uploadPDFFILE(Uri data) {
        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Uploading...!!");
        progressDialog.show();
        file=data;
        StorageReference reference=storageReference.child("uploads/"+System.currentTimeMillis()+".pdf");
        final StorageTask<UploadTask.TaskSnapshot> taskSnapshotStorageTask = reference.putFile(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();
                while (!uri.isComplete()) ;
                Uri url = uri.getResult();
                progressDialog.dismiss();
                uploaded.setText("File has been uploaded Now you can Submit...!!");
                status=1;
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress=(100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                progressDialog.setMessage("Upload:  "+(int)progress+"%");
            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == 10) {

            // Checking whether user granted the permission or not.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // Showing the toast message
                Toast.makeText(submitAssignment.this,
                        "External Storage Permission Granted",
                        Toast.LENGTH_SHORT)
                        .show();
            }

        }
    }

}