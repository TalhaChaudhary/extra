package com.example.projectsmanager;

import android.widget.DatePicker;

import java.util.Date;

public class Assignment {
    String Id,description;
    Date dateTime,postTime;

    public Assignment(String id, String description, Date dateTime, Date postTime) {
        Id = id;
        this.description = description;
        this.dateTime = dateTime;
        this.postTime = postTime;
    }

    public Assignment() {
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
