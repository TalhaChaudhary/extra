package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class AddNewStdActivity extends AppCompatActivity {

    private EditText name,password;
    private Button addedBtn;
    private String nameStr,intentGrpName,intentGrpId,passwordStr,stdIdStr;
    private  TextView groupName;
    private DatabaseReference refrence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_std);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Add New Student");
        Bundle b=getIntent().getExtras();
        refrence= FirebaseDatabase.getInstance().getReference("students");


        name= findViewById(R.id.nameStdS);
        password=findViewById(R.id.passwordStdS);
        addedBtn=findViewById(R.id.addNewStudentS);
        groupName=findViewById(R.id.groupAbsoluteS);

        Bundle bd=getIntent().getExtras();
        intentGrpName=bd.getString("grpName");
        intentGrpId=bd.getString("grpId");
        groupName.setText(intentGrpName);

    }

    public void addNewStudentS(View view) {

        nameStr=name.getText().toString();
        passwordStr=password.getText().toString();
        Long uniqueId = System.currentTimeMillis()/1000;
        stdIdStr = ""+ uniqueId;

        if(!nameStr.isEmpty() && !passwordStr.isEmpty()){
            Student s = new Student(nameStr,passwordStr,intentGrpId,stdIdStr);
            refrence.child(stdIdStr).setValue(s)
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(AddNewStdActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            new AlertDialog.Builder(AddNewStdActivity.this)
                                    .setTitle("Students")
                                    .setMessage("Studeny Added SucessFully...!")

                                    // Specifying a listener allows you to take an action before dismissing the dialog.
                                    // The dialog is automatically dismissed when a dialog button is clicked.
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            Intent in=new Intent(AddNewStdActivity.this,ViewStudentActivity.class);
                                            in.putExtra("grpId",intentGrpId);
                                            in.putExtra("grpName",intentGrpName);
                                            startActivity(in);
                                            finish();
                                        }
                                    })
                                    // A null listener allows the button to dismiss the dialog and take no further action.

                                    .show();

                            //Toast.makeText(AddNewStdActivity.this, "Student added", Toast.LENGTH_SHORT).show();

                        }
                    });
        }
        else {

            Snackbar snackbar = Snackbar
                    .make(view, "Enter Name or Password", Snackbar.LENGTH_SHORT);
            snackbar.show();
            if (nameStr.isEmpty()) {
                name.setHint("Please Enter Name");
                name.setHintTextColor(getResources().getColor(R.color.warningClr));
            }

            else if (passwordStr.isEmpty()) {
                password.setHint("Please Enter Password");
                password.setHintTextColor(getResources().getColor(R.color.warningClr));

            }

        }

    }

}
