package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AssignmentActivity extends AppCompatActivity {

    Button postAssignment;
    RecyclerView recyclerView;
    DatabaseReference reference;
    String groupId,type,suid;
    ArrayList<Assignment> assignments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Assignment");
        postAssignment=findViewById(R.id.post_assignment);
        recyclerView=findViewById(R.id.assignment_recyclerView);
        Bundle b=getIntent().getExtras();
        groupId=b.getString("groupId");
        type=b.getString("type");
        if(type.equals("student"))
        {
            suid=b.getString("suid");
            postAssignment.setVisibility(View.GONE);
        }
        assignments=new ArrayList<Assignment>();
        reference= FirebaseDatabase.getInstance().getReference("Assignments").child(groupId);
        setList();
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        AssignmentAdapter AssignmentAdapter=new AssignmentAdapter(assignments,AssignmentActivity.this,type,suid,groupId);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(AssignmentAdapter);

        postAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(AssignmentActivity.this,PostAssignmentActivity.class);
                i.putExtra("groupId",groupId);
                startActivity(i);
                finish();
            }
        });
    }
    private void setList() {
        //here we get the group Id from Intents

        reference.addListenerForSingleValueEvent(listener);
    }
    ValueEventListener listener=new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            //assignments.clear();
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                Assignment message = snapshot.getValue(Assignment.class);
                assignments.add(message);
            }
            LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
            AssignmentAdapter AssignmentAdapter=new AssignmentAdapter(assignments,AssignmentActivity.this,type,suid,groupId);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(AssignmentAdapter);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            //list.add(new Message("haseeb1","cancel ho rha hee"));
        }
    };
}