package com.example.projectsmanager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class downloadAdapter extends RecyclerView.Adapter<downloadAdapter.ViewHolder> {
    Context mContext;
    ArrayList<uploadedFile> mList;
    String groupId;

    public downloadAdapter(Context mContext, ArrayList<uploadedFile> mList, String groupId) {
        this.mContext = mContext;
        this.mList = mList;
        this.groupId = groupId;
    }

    @NonNull
    @Override
    public downloadAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.download_view, parent, false);
        return new downloadAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull downloadAdapter.ViewHolder holder, int position) {
        final uploadedFile uf=mList.get(position);
        holder.downloadText.setText("Assignment of user id : "+uf.studentId);
        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent i=new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.setData(Uri.parse(uf.getFileURL()));
                v.getContext().startActivity(i);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView downloadText;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            downloadText=itemView.findViewById(R.id.downloadTextView);
        }
    }
}
