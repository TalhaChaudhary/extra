package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class AddGroupActivity extends AppCompatActivity {

    private EditText name,password,groupName;
    private Button addedBtn,addStudentBtn,createBtn,okBtn,cancelBtn;
    private String groupNameValue,groupid,nameStr,passwordStr,stdIdStr,groupNameStr,stdList="";
    private  TextView groupNameTextView,warning;
    private RecyclerView recycle;
    private DatabaseReference refrence;
    private DatabaseReference refrenceGroup;
    private ArrayList<String> namestdList,passwordStdList,idStdList;
    StudentAdapter studentAdapter;
    String teacherId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Add Group");
        Bundle b=getIntent().getExtras();
        teacherId=b.getString("teacherId");
        refrence= FirebaseDatabase.getInstance().getReference("students");
        refrenceGroup= FirebaseDatabase.getInstance().getReference("groups").child(teacherId);

        Long uniqueId = System.currentTimeMillis()/1000;
        groupid = ""+ uniqueId;
        name= findViewById(R.id.nameStd);
        password=findViewById(R.id.passwordStd);
        addedBtn=findViewById(R.id.added);
        addStudentBtn=findViewById(R.id.addNewStudent);
        createBtn=findViewById(R.id.createGroupBtn);
        groupName= findViewById(R.id.groupName);
        groupNameTextView=findViewById(R.id.groupAbsolute);
        warning=findViewById(R.id.warningText);
        okBtn=findViewById(R.id.okBtn);
        cancelBtn=findViewById(R.id.cancelBtn);
        recycle=findViewById(R.id.recycler_view_addGroup);

        namestdList=new ArrayList<String>();
        passwordStdList=new ArrayList<String>();
        idStdList=new ArrayList<String>();

        studentAdapter =new StudentAdapter(AddGroupActivity.this,namestdList,passwordStdList,idStdList);
        recycle.setAdapter(studentAdapter);
        recycle.setLayoutManager(new LinearLayoutManager(AddGroupActivity.this));
    }

    public void Added(View view) {

        nameStr=name.getText().toString();
        passwordStr=password.getText().toString();
        Long uniqueId = System.currentTimeMillis()/1000;
        stdIdStr = ""+ uniqueId;

        if(!nameStr.isEmpty() && !passwordStr.isEmpty()){
            Student s = new Student(nameStr,passwordStr,groupid,stdIdStr);
            refrence.child(stdIdStr).setValue(s)
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(AddGroupActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            stdList=stdList + nameStr +",";
                            namestdList.add(nameStr);
                            passwordStdList.add(passwordStr);
                            idStdList.add(stdIdStr);
                            studentAdapter =new StudentAdapter(AddGroupActivity.this,namestdList,passwordStdList,idStdList);
                            recycle.setAdapter(studentAdapter);
                            recycle.setLayoutManager(new LinearLayoutManager(AddGroupActivity.this));
                        }
                    });
            createBtn.setVisibility(View.VISIBLE);
            addedBtn.setVisibility(View.INVISIBLE);
            name.setVisibility(View.INVISIBLE);
            password.setVisibility(View.INVISIBLE);
            addStudentBtn.setVisibility(View.VISIBLE);
            recycle.setVisibility(View.VISIBLE);
            name.setText("");
            password.setText("");
        }
        else {

            Snackbar snackbar = Snackbar
                    .make(view, "Enter Name or Password", Snackbar.LENGTH_SHORT);
            snackbar.show();
            if (nameStr.isEmpty()) {
                name.setHint("Please Enter Name");
                name.setHintTextColor(getResources().getColor(R.color.warningClr));
            }

            else if (passwordStr.isEmpty()) {
                    password.setHint("Please Enter Password");
                    password.setHintTextColor(getResources().getColor(R.color.warningClr));


            }

        }
    }

    public void addNewStudent(View view) {
        groupNameStr=groupName.getText().toString();
        if(!groupNameStr.isEmpty()){
            name.setVisibility(View.VISIBLE);
            password.setVisibility(View.VISIBLE);
            addedBtn.setVisibility(View.VISIBLE);
            addStudentBtn.setVisibility(View.INVISIBLE);
            groupNameValue= groupName.getText().toString();
            groupNameTextView.setText(groupNameValue);
            groupNameTextView.setVisibility(View.VISIBLE);
            groupName.setVisibility(View.INVISIBLE);
            recycle.setVisibility(View.VISIBLE);
        }
        else{
            groupName.setHint("Please Enter Name");
            groupName.setHintTextColor(getResources().getColor(R.color.warningClr));
        }


    }

    public void createGroup(View view) {
       okBtn.setVisibility(View.VISIBLE);
       cancelBtn.setVisibility(View.VISIBLE);
       warning.setVisibility(View.VISIBLE);
       password.setVisibility(View.INVISIBLE);
       name.setVisibility(View.INVISIBLE);
       recycle.setVisibility(View.INVISIBLE);
       createBtn.setVisibility(View.INVISIBLE);
       addedBtn.setVisibility(View.INVISIBLE);
       addStudentBtn.setVisibility(View.INVISIBLE);


    }

    public void OkBtn(View view) {

        Group g = new Group(groupid,groupNameStr,stdList);
        refrenceGroup.child(groupid).setValue(g)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(AddGroupActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        new AlertDialog.Builder(AddGroupActivity.this)
                                .setTitle("Student Group")
                                .setMessage("Your Group Has been Created...!")

                                // Specifying a listener allows you to take an action before dismissing the dialog.
                                // The dialog is automatically dismissed when a dialog button is clicked.
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        //Toast.makeText(AddGroupActivity.this, "Group created Succesfully", Toast.LENGTH_SHORT).show();
                                        Intent in=new Intent(AddGroupActivity.this,HomeActivity.class);
                                        startActivity(in);
                                    }
                                })
                                // A null listener allows the button to dismiss the dialog and take no further action.

                                .show();


                    }
                });
    }

    public void cancelBtn(View view) {
        okBtn.setVisibility(View.INVISIBLE);
        cancelBtn.setVisibility(View.INVISIBLE);
        warning.setVisibility(View.INVISIBLE);
        password.setVisibility(View.VISIBLE);
        name.setVisibility(View.VISIBLE);
        recycle.setVisibility(View.VISIBLE);
        createBtn.setVisibility(View.VISIBLE);
        addedBtn.setVisibility(View.VISIBLE);
        addStudentBtn.setVisibility(View.INVISIBLE);
    }
}
