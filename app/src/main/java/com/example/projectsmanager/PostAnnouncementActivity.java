package com.example.projectsmanager;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PostAnnouncementActivity extends AppCompatActivity {

    EditText description,TitleText;
    Button post;
    DatabaseReference reference;
    Date date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_announcement);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Post Announcement");
        description=findViewById(R.id.descriptionText);
        post=findViewById(R.id.okay_post);
        //setDate=findViewById(R.id.set_date);
        date=new Date();
        String s=date+"";
        //setDate.setText(s.substring(0,10));
        TitleText=findViewById(R.id.TitleText);
        Bundle b=getIntent().getExtras();
        //getting extras from intent
        final String groupId=b.getString("groupId");
        TitleText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(TitleText.getText().toString().length()>0)
                {
                    TitleText.setTextColor(Color.BLACK);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(description.getText().toString().length()>0)
                    {
                        description.setTextColor(Color.BLACK);
                    }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        reference= FirebaseDatabase.getInstance().getReference("Announcement").child(groupId);
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
                String format = simpleDateFormat.format(new Date());
                if(TitleText.getText().toString().length()<1)
                {
                    TitleText.setHint("Please Fill this Field");
                    TitleText.setTextColor(Color.RED);
                }
                else if(description.getText().toString().length()<1)
                {
                    description.setHint("Please Fill this field First");
                    description.setTextColor(Color.RED);
                }
                else {
                    date =new Date();
                    Announcement a = new Announcement(format, description.getText().toString(),TitleText.getText().toString(),date);
                    reference.child(groupId + format).setValue(a);
                    new AlertDialog.Builder(PostAnnouncementActivity.this)
                            .setTitle("Announcement")
                            .setMessage("Your Announcement Has been sent to the Students")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Intent i = new Intent(PostAnnouncementActivity.this, AnnouncementActivity.class);
                            i.putExtra("groupId", groupId);
                            i.putExtra("type", "teacher");
                            startActivity(i);
                            finish();
                        }
                    })
                            // A null listener allows the button to dismiss the dialog and take no further action.

                            .show();

                }
            }
        });
    }
    private void chooseDate() {
        final Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker =
                new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(final DatePicker view, final int year, final int month,
                                          final int dayOfMonth) {

                        @SuppressLint("SimpleDateFormat")
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        calendar.set(year, month, dayOfMonth);
                        //date=calendar.getTime();
                        String dateString = sdf.format(calendar.getTime());
                        date=new Date(year,month,dayOfMonth,12,00,00);
                        String temp=date+"";
                       // setDate.setText(temp.substring(0,10));
                        //date_field.setText(dateString); // set the date
                    }
                }, year, month, day); // set date picker to current date

        datePicker.show();

        datePicker.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(final DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }
}