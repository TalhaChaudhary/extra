package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ViewStudentActivity extends AppCompatActivity {

    private TextView grpName,load;
    private Button vStd,addStd;
    private RecyclerView recycler;
    private ViewAllStudentsAdapter viewStdAdapter;
    private ArrayList<String> namestdList,passwordStdList,idStdList;
    private String groupIdVal,groupNameVal;
    private DatabaseReference reference3;
    private ArrayList<Student> sList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("View Student");
        reference3 = FirebaseDatabase.getInstance().getReference("students");
        grpName=findViewById(R.id.viewStdentText);
        recycler=findViewById(R.id.recycler_view_Students);
        addStd= findViewById(R.id.AddStdGroup);
        Bundle b=getIntent().getExtras();
        groupIdVal=b.getString("grpId");
        groupNameVal=b.getString("grpName");
        grpName.setText(groupNameVal);

        namestdList = new ArrayList<>();
        passwordStdList = new ArrayList<>();
        idStdList = new ArrayList<>();
        sList= new ArrayList<>();

        viewStdAdapter = new ViewAllStudentsAdapter(ViewStudentActivity.this, namestdList,passwordStdList,idStdList,groupIdVal,groupNameVal);
        recycler.setAdapter(viewStdAdapter);
        recycler.setLayoutManager(new LinearLayoutManager(ViewStudentActivity.this));



      reference3.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    sList.add(postSnapshot.getValue(Student.class));
                }
                namestdList.clear();
                passwordStdList.clear();
                idStdList.clear();
                for(Student std:sList){
                    if(std.getGrpId().equals(groupIdVal)){
                        namestdList.add(std.getName());
                        passwordStdList.add(std.getPassword());
                        idStdList.add(std.getStdId());
                    }
                }
                viewStdAdapter = new ViewAllStudentsAdapter(ViewStudentActivity.this, namestdList,passwordStdList,idStdList,groupIdVal,groupNameVal);
                recycler.setAdapter(viewStdAdapter);
                recycler.setLayoutManager(new LinearLayoutManager(ViewStudentActivity.this));


            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ViewStudentActivity.this, databaseError.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void AddStudentsFunc(View view) {
        Intent i=new Intent(ViewStudentActivity.this, AddNewStdActivity.class);
        i.putExtra("grpName",groupNameVal);
        i.putExtra("grpId",groupIdVal);
        startActivity(i);
        finish();
    }
}
