package com.example.projectsmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
public class signupActivity extends AppCompatActivity {

    private TextView name, email, password, conPassword;
    private String nameStr, passwordStr, emailStr, conPasswordStr;
    private DatabaseReference refrence;
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Sign Up");
        refrence= FirebaseDatabase.getInstance().getReference("teachers");
        auth= FirebaseAuth.getInstance();

        name= findViewById(R.id.nameFiels);
        email= findViewById(R.id.emailFiels);
        password= findViewById(R.id.passwordFeild);
        conPassword= findViewById(R.id.confirmpasswordFeild);

    }
    public void sigupFun(View view) {

        nameStr= name.getText().toString().toLowerCase();
        passwordStr= password.getText().toString();
        emailStr= email.getText().toString();
        conPasswordStr= conPassword.getText().toString();
    if (!nameStr.isEmpty() && !passwordStr.isEmpty() && !emailStr.isEmpty() && !conPasswordStr.isEmpty()){

        if(passwordStr.equals(conPasswordStr)) {
            auth.createUserWithEmailAndPassword(emailStr,passwordStr)
                    .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            teacher t = new teacher(nameStr,emailStr,passwordStr ,auth.getCurrentUser().getUid());
                            refrence.child(nameStr).setValue(t)
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(signupActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                        }
                                    })
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(signupActivity.this, "Data Sucessfully added", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(signupActivity.this, MainActivity.class));
                                        }
                                    });

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(signupActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });

        }
        else{
            conPassword.setText("");
            conPassword.setHint("Not Matched");
            conPassword.setHintTextColor(getResources().getColor(R.color.warningClr));
        }
    }
    else{
        if(nameStr.isEmpty()){
            name.setHint("Please Enter Name");
            name.setHintTextColor(getResources().getColor(R.color.warningClr));
        }
        else if(emailStr.isEmpty()){
            email.setHint("please Enter Email");
            email.setHintTextColor(getResources().getColor(R.color.warningClr));

        }

      else if(passwordStr.isEmpty()){
            password.setHint("please Enter Password");
           password.setHintTextColor(getResources().getColor(R.color.warningClr));
        }

      else if(conPasswordStr.isEmpty()){
            conPassword.setHint("Please enter Comfirm");
            conPassword.setHintTextColor(getResources().getColor(R.color.warningClr));

        }



    }

    }
}
