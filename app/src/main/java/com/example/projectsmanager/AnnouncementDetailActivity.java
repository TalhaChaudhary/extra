package com.example.projectsmanager;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.TextView;

public class AnnouncementDetailActivity extends AppCompatActivity {

    TextView date,title,description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement_detail);
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#0020C2"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Announcement Detail");
        date=findViewById(R.id.announcementdate);
        description=findViewById(R.id.announcementdescription);
        title=findViewById(R.id.announcementtitle);
        Bundle b=getIntent().getExtras();
        date.setText(b.getString("date"));
        title.setText(b.getString("title"));
        description.setText(b.getString("description"));
    }
}