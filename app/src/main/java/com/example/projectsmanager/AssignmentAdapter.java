package com.example.projectsmanager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Date;

public class AssignmentAdapter extends RecyclerView.Adapter<AssignmentAdapter.ViewHolder> {
    ArrayList<Assignment> mList;
    Context mContext;
    String type,suid,groupId;

    public AssignmentAdapter(ArrayList<Assignment> mList, Context mContext, String type, String suid, String groupId) {
        this.mList = mList;
        this.mContext = mContext;
        this.type = type;
        this.suid = suid;
        this.groupId = groupId;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.assignment_view, parent, false);
        return new AssignmentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            final Assignment a=mList.get(position);
            holder.assignment.setText(a.getDescription());
            holder.date.setText("Due Date : "+a.getDateTime().toString().substring(0,10));
            holder.postDate.setText("Post Date : "+a.getPostTime().toString().substring(0,10));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(type.equals("student")) {
                        //toh ise kisi activity pe le jana or sath me pass krna he assignment ki id or student jo file upload kre ga wo 1
                        // wo uploaded files k name se firebase me 1 node bne ga jaha url or assignment id save hogi jb upload ho jaye ga
                        //or intent k through hm ne student ki id b save krwani he or url b
                        // }
                        Date currentDate=new Date();
                        if(currentDate.getDate()>a.getDateTime().getDate())
                        {
                            new AlertDialog.Builder(mContext)
                                    .setTitle("Assignment")
                                    .setMessage("Your time for assignment submission is over...!")

                                    // Specifying a listener allows you to take an action before dismissing the dialog.
                                    // The dialog is automatically dismissed when a dialog button is clicked.
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int whichButton) {

                                        }
                                    })
                                    // A null listener allows the button to dismiss the dialog and take no further action.

                                    .show();

                        }
                        else
                        {
                            Intent i=new Intent(mContext,submitAssignment.class);
                            i.putExtra("assignmentId",a.getId());
                            i.putExtra("groupId",groupId);
                            i.putExtra("suid",suid);
                            v.getContext().startActivity(i);
                        }
                        //Toast.makeText(mContext,type +"  :  "+ suid+" :  "+groupId,Toast.LENGTH_LONG).show();

                    }
                    else if(type.equals("teacher"))
                    {
                        //yeh sirf URL daikhe ga or click hone pe download ho jaye gaa
                        Intent i=new Intent(mContext,downloadAssignment.class);
                        i.putExtra("assignmentId",a.getId());
                        i.putExtra("groupId",groupId);
                        v.getContext().startActivity(i);

                    }
                }
            });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView assignment,date,postDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            assignment=itemView.findViewById(R.id.assignment);
            postDate=itemView.findViewById(R.id.assignmentPostDate);
            date=itemView.findViewById(R.id.dateAssignment);
        }
    }
}
